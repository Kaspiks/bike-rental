 <?php  
require 'config.php';
require 'functions.php';
$conn = Connect();
session_start();
?>

               
<?php
 include 'header.php';

        $bike_id = $_GET["id"];
        $sql1 = "SELECT * FROM bikes WHERE bike_id = '$bike_id'";
        $result1 = mysqli_query($conn, $sql1);

        if(mysqli_num_rows($result1)){
            while($row = mysqli_fetch_assoc($result1)){
                $bike_id = $row['bike_id'];
                $bike_name = $row["name"];
                $content = $row["content"];
                $bike_type = $row["type"];
                $bike_img = $row["bike_img"];
                $bike_menu_name = $row["menu_name"];
                $bike_url = $row["url"];
                $bike_teaser = $row["teaser"];
                $bike_meta_title = $row["meta_title"];
                $bike_meta_keywords = $row["meta_keywords"];
                $bike_meta_description = $row["meta_description"];;
            }
        }

        if(isset($_POST['update_bike'])) {
        
        
            $bike_name           =  escape($_POST['name']);
            $content             =  escape($_POST['content']);
            $bike_type    =  escape($_POST['type']);
            $file_name=$_FILES["uploadedimage"]["name"];
            $temp_name=$_FILES["uploadedimage"]["tmp_name"];
            $bike_menu_name          =  escape($_POST['menu_name']);
            $bike_url     =  escape($_POST['url']);
            $bike_teaser        =  escape($_POST['teaser']);
            $bike_meta_title           =  escape($_POST['meta_title']);
            $bike_meta_keywords = escape($_POST['meta_keywords']);
            $bike_meta_description = escape($_POST['meta_description']);
            $imagename=$_FILES["uploadedimage"]["name"];

            $target_path = "images/bikes/".$imagename;

            move_uploaded_file($temp_name, $target_path); 
            
            if(empty($target_path)) {
            
            $query = "SELECT * FROM bikes WHERE bike_id = $bike_id ";
            $select_image = mysqli_query($conn,$query);
                
            while($row = mysqli_fetch_array($select_image)) {
                
               $target_path = $row['bike_img'];
            
            }
            
            
    }
            $bike_name = mysqli_real_escape_string($conn, $bike_name);
    
            
              $query = "UPDATE bikes SET ";
              $query .="name  = '{$bike_name}', ";
              $query .="content  = '{$content}', ";
              $query .="type = '{$bike_type}', ";
              $query .="bike_img = '{$target_path}', ";
              $query .="menu_name = '{$bike_menu_name}', ";
              $query .="url = '{$bike_url}', ";
              $query .="teaser   = '{$bike_teaser}', ";
              $query .="meta_title= '{$bike_meta_title}', ";
              $query .="meta_keywords  = '{$bike_meta_keywords}', ";
              $query .="meta_description  = '{$bike_meta_description}' ";
              $query .= "WHERE bike_id = {$bike_id} ";
            
            $update_post = mysqli_query($conn,$query);
            
            confirmQuery($update_post);
            
            echo "<p class='bg-success'>Post Updated. <a href='bike?id={$bike_id}'>View Post </a> or <a href='manage-bikes.php'>Edit More Posts</a></p>";
            
        
        
}
        
        ?>

<script>

var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

tinymce.init({
  selector: 'textarea#full-featured-non-premium',
  plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
  imagetools_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: true,
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  branding: false,
  protect: [
    /<\?php[\s\S]*?\?>/g // Protect php code
],
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  file_picker_callback: function (callback, value, meta) {
    /* Provide file and text for the link dialog */
    if (meta.filetype === 'file') {
      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    }

    /* Provide image and alt text for the image dialog */
    if (meta.filetype === 'image') {
      callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
    }

    /* Provide alternative source and posted for the media dialog */
    if (meta.filetype === 'media') {
      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    }
  },
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 300,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  contextmenu: 'link image imagetools table',
  skin: "oxide-dark",
content_css: "dark",
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
 });


    </script>
      <!-- Navigation -->


    <div class="container rental-container" style="margin-top: 65px;" >
    <div class="col-md-7" style="float: none; margin: 0 auto;">
      <div class="form-area">
        <form  class="rental-form" role="form" action="" enctype="multipart/form-data" method="POST">
        <br style="clear: both">
          <h3 style="margin-bottom: 25px; text-align: center; font-size: 30px;"> Please Provide Your Bike Details. </h3>

          <div class="form-group">
          <label for="name">Riteņa nosaukums</label>
            <input  value="<?php echo htmlspecialchars(stripslashes($bike_name)); ?>" type="text" class="form-control" id="name" name="name"  required autofocus="">
          </div>


          <div class="form-group">
         <label for="content">Riteņa apraksts</label>
         <form method="post">
      <textarea id="full-featured-non-premium" name= "content">
      <?php echo $content; ?>

    </textarea>
    </form> 

          <div class="form-group">
          <label for="type">Riteņa tips</label>
            <select name="type" class="form-control" id="type" placeholder="type" required>
              <option selected value="<?php echo $bike_type; ?>"><?php echo $bike_type?></option>
              <option value="Komforta">Komforta</option>
              <option value="Hibrīds">Hibrīds</option>
              <option value="Kalna">Kalna</option>
            </select>
          </div>

          <div class="form-group">
          <label for="menu_name">Menu nosaukums</label>
            <input  value="<?php echo $bike_menu_name; ?>" type="text" class="form-control" id="menu_name" name="menu_name" placeholder="menu_name" required>
          </div>

          <div class="form-group">
          <label for="url">URL adrese</label>
            <input value="<?php echo $bike_url; ?>" type="text" class="form-control" id="url" name="url" placeholder="url" required>
          </div>

          <div class="form-group">
          <label for="teaser">Ieskats tekstā</label>
            <input  value="<?php echo $bike_teaser; ?>" type="text" class="form-control" id="teaser" name="teaser" placeholder="teaser" required>
          </div>
          <div class="form-group">
          <label for="meta_title">Meta virsraksts</label>
            <input value="<?php echo $bike_meta_title; ?>"  type="text" class="form-control" id="meta_title" name="meta_title" placeholder="meta_title" required>
          </div>
          <div class="form-group">
          <label for="meta_keywords">Meta atslēgas vārdi</label>
            <input value="<?php echo $bike_meta_keywords; ?>" type="text" class="form-control" id="meta_keywords" name="meta_keywords" placeholder="meta_keywords" required>
          </div>
          <div class="form-group">
          <label for="meta_description">Meta apraksts</label>
            <input value="<?php echo $bike_meta_description; ?>" type="text" class="form-control" id="meta_description" name="meta_description" placeholder="meta_description" required>
          </div>

          <div class="form-group img-input">
            <div class="inputfile-box">
            <img width="100" src="<?php echo $bike_img; ?>" alt="">
              <input name="uploadedimage" type="file" id="file" class="inputfile" onchange='uploadFile(this)'>
              <label for="file">
                <span id="file-name" class="file-box"></span>
                <span class="file-button">
                  <i class="fa fa-upload" aria-hidden="true"></i>
                  Select File
                </span>
              </label>
            </div>
          </div>
           <button type="submit" id="submit" name="update_bike" value="Update Bike" class="button primary rent"> Izmainīt riteņa rakstu</button>    
        </form>
      </div>
    </div>
    </div>
</div>
<?php include 'footer.php';