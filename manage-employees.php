
<?php
require 'config.php';
$conn = Connect();
session_start();

if(isset($_POST['checkBoxArray'])) {
    foreach($_POST['checkBoxArray'] as $employeeValueId ){
        $bulk_options = $_POST['bulk_options'];
        switch($bulk_options) {
            case 'delete':
                $query = "DELETE FROM employees WHERE employee_id = {$employeeValueId}  ";
                $update_to_delete_status = mysqli_query($conn,$query);
                break;
            case 'yes-availability':
                $query = "SELECT * FROM employees WHERE employee_id = '{$employeeValueId}' ";
                $update_to_available = mysqli_query($conn, $query);
                while ($row = mysqli_fetch_array($update_to_available)) {
                    $employee_name = $row["employee_name"];
                    $employee_surname = $row["employee_surname"];
                    $email = $row["email"];
                    $phone = $row["phone"];
                    $employee_availability = $row["employee_availability"];
                }
                if($employee_availability = "yes"){
                    $query = "UPDATE `employees` SET `employee_availability` = 'no' WHERE `employees`.`employee_id` = {$employeeValueId}";
                }else {
                    $query = "UPDATE `employees` SET `employee_availability` = 'yes' WHERE `employees`.`employee_id` = {$employeeValueId}";

                }
                    break;
            case 'clone':
                $query = "SELECT * FROM employees WHERE employee_id = '{$employeeValueId}' ";
                $select_post_query = mysqli_query($conn, $query);
                while ($row = mysqli_fetch_array($select_post_query)) {
                    $employee_name = $row["employee_name"];
                    $employee_surname = $row["employee_surname"];
                    $email = $row["email"];
                    $phone = $row["phone"];
                    $employee_availability = $row["employee_availability"];

                }
                $query = "INSERT into bikes(employee_name,employee_surname,email,phone,employee_availability) VALUES('" . $employee_name . "','" . $employee_surname . "','".$email."','" . $phone . "','" . $employee_availability . "')";
                $copy_query = mysqli_query($conn, $query);

                if(!$copy_query ) {

                    die("QUERY FAILED" . mysqli_error($conn) . $query);
                }
                break;
        }
    }
}
include 'header.php'
?>

    <form class="manage-b" action="" method='post'>
        <table class="table table-bordered table-hover">
            <div id="bulkOptionContainer">
                <select class="form-control" name="bulk_options" id="">
                <option value="">Izvēlieties opciju</option>
                <option value="delete">Dzēst</option>
                <option value="yes-availability">Jā-Pieejamība</option>
                <option value="clone">Klonēt</option>
            </select>
            </div>
            <div class="col-xs-4">
                <input type="submit" name="submit" class="btn btn-success btn-ap bg" value="Apstiprināt">
                <a class="btn-add" href="enterdriver.php">Pievienot</a>
            </div>
            <thead>
                <tr>
                    <th><input id="selectAllBoxes" type="checkbox"></th>
                    <th>Id</th>
                    <th> Vārds</th>
                    <th> Uzvārds </th>
                    <th> Telefona numurs  </th>
                    <th> E-pasts </th>
                    <th> Pieejamība </th>
                    <th> Rediģēt</th>
                </tr>
            </thead>

            <tbody>
                <?php
        $query = "SELECT * FROM employees ORDER BY employee_id DESC ";
        $select_bikes = mysqli_query($conn,$query);
        while($row = mysqli_fetch_assoc($select_bikes )) {
            $employee_id = $row['employee_id'];
            $employee_name = $row["employee_name"];
            $employee_surname = $row["employee_surname"];
            $email = $row["email"];
            $phone = $row["phone"];
            $employee_availability = $row["employee_availability"];
            echo "<tr>";
            ?>    

   <td class="cbx"><input id="cbx" type='checkbox' name='checkBoxArray[]' value='<?php echo $employee_id; ?>'  /><label for="cbx"></label><svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                <path d="M2 8.36364L6.23077 12L13 2"></path>
            </svg></td>
            <defs>
                <filter id="goo">
                    <fegaussianblur in="SourceGraphic" stddeviation="4" result="blur"></fegaussianblur>
                    <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 22 -7" result="goo"></fecolormatrix>
                    <feblend in="SourceGraphic" in2="goo"></feblend>
                </filter>
            </defs>
        </svg>
                        <td><?php echo $row["employee_id"]; ?></td>
                        <td><?php echo $row["employee_name"]; ?></td>
                        <td><?php echo $row["employee_surname"]; ?></td>
                        <td><?php echo $row["phone"]; ?></td>
                        <td><?php echo $row["email"]; ?></td>
                        <td><?php echo $row["employee_availability"]; ?></td>
                        <td>
                            <a class='btn-add sm' href='edit-employee.php?id=<?php echo $employee_id;?>'>Rediģēt</a>
                        </td>
            </tr>
            <?php

        }

        ?>
            </tbody>
        </table>
    </form>
    </div>

    <?php
include 'footer.php';?>