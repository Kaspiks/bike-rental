<html>

  <head>
    <title> customer Signup | Car Rentals </title>
  </head>
  <body>
    <!-- Navigation -->
    

<?php
session_start();

require 'config.php';
$conn = Connect();

function GetImageExtension($imagetype) {
    if(empty($imagetype)) return false;
    
    switch($imagetype) {
        case 'assets/img/cars/bmp': return '.bmp';
        case 'assets/img/cars/gif': return '.gif';
        case 'assets/img/cars/jpeg': return '.jpg';
        case 'assets/img/cars/png': return '.png';
        default: return false;
    }
}

$name = $conn->real_escape_string($_POST['name']);
$content = $conn->real_escape_string($_POST['content']);
$type = $conn->real_escape_string($_POST['type']);
$menu_name = $conn->real_escape_string($_POST['menu_name']);
$url = $conn->real_escape_string($_POST['url']);
$teaser = $conn->real_escape_string($_POST['teaser']);
$meta_title = $conn->real_escape_string($_POST['meta_title']);
$meta_keywords = $conn->real_escape_string($_POST['meta_keywords']);
$meta_description = $conn->real_escape_string($_POST['meta_description']);

//$query = "INSERT into cars(car_name,car_nameplate,ac_price,non_ac_price,car_availability) VALUES('" . $car_name . "','" . $car_nameplate . "','" . $ac_price . "','" . $non_ac_price . "','" . $car_availability ."')";
//$success = $conn->query($query);


if (!empty($_FILES["uploadedimage"]["name"])) {
    $file_name=$_FILES["uploadedimage"]["name"];
    $temp_name=$_FILES["uploadedimage"]["tmp_name"];
    $imgtype=$_FILES["uploadedimage"]["type"];
    $ext= GetImageExtension($imgtype);
    $imagename=$_FILES["uploadedimage"]["name"];
    $target_path = "images/bikes/".$imagename;

    if(move_uploaded_file($temp_name, $target_path)) {
        //$query0="INSERT into cars (car_img) VALUES ('".$target_path."')";
      //  $query0 = "UPDATE cars SET car_img = '$target_path' WHERE ";
        //$success0 = $conn->query($query0);

        $query = "INSERT into bikes(name,content,type,bike_img,menu_name,url,teaser,meta_title,meta_keywords,meta_description) VALUES('" . $name . "','" . $content . "','".$type."','" . $target_path . "','" . $menu_name . "','" . $url . "','" . $teaser . "','" . $meta_title ."','" . $meta_keywords ."','" . $meta_description ."')";
        $success = $conn->query($query);

        
    } 

}


// Taking car_id from cars

$query1 = "SELECT id from bikes where name = '$name'";

$result = mysqli_query($conn, $query1);
$rs = mysqli_fetch_array($result, MYSQLI_BOTH);
$id = $rs['id'];
 

$query2 = "INSERT into employeebikes(id,employee_name) values('" . $id ."','" . $_SESSION['username'] . "')";
$success2 = $conn->query($query2);

if (!$success){ ?>
    <div class="container">
	<div class="jumbotron" style="text-align: center;">
        Car with the same vehicle number already exists!
        <?php echo $conn->error; ?>
        <br><br>
        <a href="enterbike.php" class="btn btn-default"> Go Back </a>
</div>
<?php	
}
else {
    header("location: enterbike.php"); //Redirecting 
}

$conn->close();

?>

    </body>
    <footer class="site-footer">
        <div class="container">
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    <h5>© <?php echo date("Y"); ?> Car Rentals</h5>
                </div>
            </div>
        </div>
    </footer>
</html>