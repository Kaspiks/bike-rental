<!--  <?php  
require 'config.php';
$conn = Connect();
session_start();
?>

               
<?php
        $bike_id = $_GET["id"];
        $sql1 = "SELECT * FROM bikes WHERE bike_id = '$bike_id'";
        $result1 = mysqli_query($conn, $sql1);

        if(mysqli_num_rows($result1)){
            while($row1 = mysqli_fetch_assoc($result1)){
                $bike_name = $row1["name"];
                $bike_type = $row1["type"];
                $bike_teaser = $row1["teaser"];
                $bike_img = $row1["bike_img"];
                $bike_content = $row1["content"];
                $url = $row1["url"];
                $meta_title = $row1["meta_title"];
                $meta_keywords = $row1["meta_keywords"];
                $meta_description = $row1["meta_description"];
            }
        }

        ?>



    
 
    <!-- Page Content -->
    <!DOCTYPE html>

    <html>
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
            <title><?php if (!empty($meta_title)) {echo $meta_title;}else {echo $bike_name;}?></title>
	        <meta name="description" content="<?php if (!empty($meta_description)) {echo $meta_description;}else {echo $bike_teaser;}?>" /> 
	        <meta name="keywords" content="<?php if (!empty($meta_keywords)) {echo $meta_keywords;}else {echo $bike_name;}?>" />
	
            <link rel="stylesheet" href="assets/css/main.css" />
            <link rel="stylesheet" href="https://fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
            <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
        </head>
        <body class="landing is-preload" id="lock">
            <div id="page-wrapper">
                <header id="header">
                    <h1><a href="index.php">Riteņu noma</a></h1>
                    <nav id="nav">
                <ul>
                    <li class="special">
                        <a href="#menu" class="menuToggle"><span>Menu</span></a>
                        <div id="menu">
                            <ul>
                                <?php
                                if(isset($_SESSION["loggedin"])){
                                    ?>
                                <li><a href="index">Home</a></li>
                                <li><a href="generic">Generic</a></li>
                                <li><a href="elements">Elements</a></li>
                                    <li class="dropbtn" onclick="myFunction('m1')">Menu</li>
                                    <div class="dropdown-content" id="m1">
                                        <li class="dropbtn" onclick="myFunction('m2')">Riteņi</li>
                                        <div class="dropdown-content" id="m2">
                                            <a href="manage-bikes">Pārskatīt riteņus</a>
                                            <a href="enterbike">Pievienot riteni</a>
                                        </div>
                                        <li class="dropbtn" onclick="myFunction('m3')">Braucēji</li>
                                        <div class="dropdown-content" id="m3">
                                            <a href="manage-employees">Pārskatīt braucējus</a>
                                            <a href="enterdriver">Pievienot braucēju</a>
                                        </div>
                                        <li class="dropbtn" onclick="myFunction('m4')">Iznomātie riteņi</li>
                                        <div class="dropdown-content" id="m4">
                                            <a href="manage-rentals">Pārskatīt nomas līgumus</a>
                                        </div>
                                    </div>
                                <li><a href="logout.php">Log out</a></li>
                                <li><a href="#">Welcome <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a></li>
                                                                <?php
                                } else{
                                ?>
                                <li><a href="index.php">Home</a></li>
                                    <li><a href="generic.php">Generic</a></li>
                                    <li><a href="elements.php">Elements</a></li>
                                    <li><a href="newlogin2.php">Pieslēgties</a></li>
                                    <?php
                                }
                                ?>       
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
                </header>
                    <!-- Footer -->
  





        <div class="bike wrapper style4">

            <!-- Blog Entries Column -->
            


        <div class="bike-photo">
            <img src="<?php echo $bike_img?>"
        </div>
        <div class="bike-detail">
            <h1 class="bike__title"><?php echo $bike_name?></h1>
            <div class="bike__type"><?php echo $bike_type?></div>
            <div class="bike__subtitle"><?php echo $bike_teaser?></div>

        </div>


        </div>

    </div>
    <div class="extra_info">
            <?php echo $bike_content?>
            <div id="abc">
        <!-- Popup Div Starts Here -->
        <div id="booking" class="section">
            <div class="section-center">
                <div class="container">
                    <div class="row-booking">
                        <div class="booking-form">
                            <img id="close" src="/assets/images" onclick="div_hide()">
                            <div class="booking-bg">
                                <div class="form-header">
                                    <h2>Make your reservation</h2>
                                </div>
                            </div>
                            <form action="bookingconfirm.php" method="POST">
                                <div class="row">
                                <?php $today = date("Y-m-d") ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="form-label">Nomas sākuma datums</span>
                                            <input class="form-control" type="date" name="rent_start_date" min="<?php echo($today);?>"  required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="form-label">Nomas beigu datums</span>
                                            <input class="form-control" type="date" name="rent_end_date" min="<?php echo($today);?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="form-label">Darbinieks</span>
                                    <select class="form-control" name="employee_id_from_dropdown" ng-model="myVar1" required>
                                        <option value="" selected hidden>Izvēlieties darbinieku kam piešķirt šo riteni</option>

                                    <?php
                                    $sql2 = "SELECT * FROM employees  WHERE employee_availability = 'yes'";
                                    $result2 = mysqli_query($conn, $sql2);

                                    if(mysqli_num_rows($result2) > 0){
                                        while($row2 = mysqli_fetch_assoc($result2)){
                                            $employee_id = $row2["employee_id"];
                                            $employee_name = $row2["employee_name"];
                                            $employee_surname = $row2["employee_surname"];
                                            $employee_email = $row2["email"];
                                            $employee_phone = $row2["phone"];
                                        ?>
                                        <option value="<?php echo($employee_id); ?>"><?php echo($employee_name); ?>
                                        <?php }} 
                                        else{
                                            ?>
                                        Sorry! No Drivers are currently available, try again later...
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span class="select-arrow"></span>

                                    <div ng-switch="myVar1">
                                        <?php
                                    $sql3 = "SELECT * FROM employees e WHERE e.employee_availability = 'yes'";
                                    $result3 = mysqli_query($conn, $sql3);

                                                if(mysqli_num_rows($result3) > 0){
                                                    while($row3 = mysqli_fetch_assoc($result3)){
                                                        $employee_id = $row3["employee_id"];
                                                        $employee_name = $row3["employee_name"];
                                                        $employee_surname = $row3["employee_surname"];
                                                        $employee_email = $row3["email"];
                                                        $employee_phone = $row3["phone"];
                                        ?>
<!--
                                        <div ng-switch-when="<?php echo($employee_id); ?>">
                                            <h5>Employee Name:&nbsp; <b><?php echo($employee_name); ?></b></h5>
                                            <p>Employee Surname:&nbsp; <b><?php echo($employee_surname); ?></b> </p>
                                            <p>Contact Email:&nbsp; <b><?php echo($employee_email); ?></b> </p>
                                            <p>Contact Phone:&nbsp; <b><?php echo($employee_phone); ?></b> </p>-->
                                        <?php }} ?>
                                        </div>
                                    </div>
                                    <input type="submit"name="submit" value="Nomā tagad" class="submit-btn">     
                                </div>
                                <input type="hidden" name="hidden_bikeid" value="<?php echo $bike_id; ?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Popup Div Ends Here -->

    </div>
    <div>
        <button id="popup" class="product__button" onclick="div_show()">Popup</button>
    </div>
    </div>
  

            
        <footer id="footer">
            <ul class="icons">
                <li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
                <li><a href="#" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
            </ul>
            <ul class="copyright">
                <li>&copy; Riteņu noma</li>
                <li>Uzziniet vairāk šeit: <a href="index.php" class="fas fa-question"><span class="label"></span></a></li>

            </ul>
        </footer>
        </div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/script.js"></script>

    </body>
</html>

            <!-- Blog Sidebar Widgets Column -->
            
            
             
        <!-- /.row -->


   

