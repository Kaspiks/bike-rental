<?php

require_once "config.php";

$name = $content = $type = $menu_name = $url = $teaser = $meta_title = $meta_keywords = $meta_description = "";
$name_err = $content_err = $type_err = $menu_name_err = $url_err = $teaser_err = $meta_title_err = $meta_keywords_err = $meta_description_err = "";




if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(empty(trim($_POST["name"]))){
        $name_err = "Lūdzu ievadiet raksta nosaukumu";
    }else{
        $sql = "SELECT id from bikes WHERE name = ?";

        
        if($stmt = $mysqli->prepare($sql)){
            $stmt->bind_param("s", $param_name);

            $param_name = trim($_POST["name"]);

            if($stmt->execute()){
                $stmt->store_result();

                if($stmt->num_rows == 1){
                    $name_err = "This name is already taken.";
                } else{
                    $name = trim($_POST["name"]);
                }
            } else{
                    echo "OOPS! Kaut kas nogāja gerizi. Lūdzu mēģiniet atkal vēlāk.";
                }

            $stmt->close();
            
        }

    }

    if(empty(trim($_POST["content"]))){
        $content_err = "Lūdzu ievadiet saturu.";
    } else {
        $content = trim($_POST["content"]);
    }

    if(empty(trim($_POST["type"]))){
        $type_err = "Lūdzu ievadiet riteņa tipu.";
    } else {
        $type = trim($_POST["type"]);
    }
    $statusMsg = '';

    // File upload path
    $targetDir = "/images";
    $fileName = basename($_FILES["file"]["name"]);
    $targetFilePath = $targetDir . $fileName;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

    if(empty(trim($_POST["menu_name"]))){
        $menu_name_err = "Lūdzu ievadiet menu vārdu.";
    } else {
        $menu_name = trim($_POST["menu_name"]);
    }

    if(empty(trim($_POST["url"]))){
        $url_err = "Lūdzu ievadiet mājaslapas url.";
    } else {
        $url = trim($_POST["url"]);
    }

    if(empty(trim($_POST["teaser"]))){
        $teaser_err = "Lūdzu ievadiet lapas teaseri.";
    } else {
        $teaser = trim($_POST["teaser"]);
    }

    if(empty(trim($_POST["meta_title"]))){
        $meta_title_err = "Lūdzu ievadiet lapas meta titulu.";
    } else {
        $meta_title = trim($_POST["meta_title"]);
    }

    if(empty(trim($_POST["meta_keywords"]))){
        $meta_keywords_err = "Lūdzu ievadiet lapas meta atslēgas vārdus.";
    } else {
        $meta_keywords = trim($_POST["meta_keywords"]);
    }

    if(empty(trim($_POST["meta_description"]))){
        $meta_description_err = "Lūdzu ievadiet lapas meta aprakstu.";
    } else {
        $meta_description = trim($_POST["meta_description"]);
    }





    if(empty($name_err) && empty($content_err) && empty($type_err) && empty($menu_name_err) && empty($url_err) && empty($teaser_err) && empty($meta_title_err) && empty($meta_keywords_err) && empty($meta_description_err)){
        

    
        $sql = "INSERT INTO bikes (name, content, type,bike_img ,menu_name ,url ,teaser ,meta_title ,meta_keywords ,meta_description) VALUES (?, ?, ?,'".$fileName."', ?, ?, ?, ?, ?, ?)";

        if($stmt = $mysqli->prepare($sql)){

            $stmt->bind_param("sssssssss", $param_name, $param_content, $param_type, $param_menu_name, $param_url, $param_teaser, $param_meta_title, $param_meta_keywords, $param_meta_description);


            $param_name = $name;
            $param_content = $content;
            $param_type = $type;
            $param_menu_name = $menu_name;
            $param_url = $url;
            $param_teaser = $teaser;
            $param_meta_title = $meta_title;
            $param_meta_keywords = $meta_keywords;
            $param_meta_description = $meta_description;



            if($stmt->execute()){
                header("location: index.php");
            } else {
                echo "OOPS! Kaut kas nogāja greizi. Lūdzu mēģiniet vēlreiz vēlāk."; 
            }

            $stmt->close();
        }
    }


    $mysqli->close();


}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; }
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Bike name</label>
                <input type="text" name="name" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                <span class="invalid-feedback"><?php echo $name_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Bike description</label>
                <input type="text" name="content" class="form-control <?php echo (!empty($content_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $content; ?>">
                <span class="invalid-feedback"><?php echo $content_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Bike type</label>
                <input type="text" name="type" class="form-control <?php echo (!empty($type_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $type; ?>">
                <span class="invalid-feedback"><?php echo $type_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Bike menu name</label>
                <input type="text" name="menu_name" class="form-control <?php echo (!empty($menu_name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $menu_name; ?>">
                <span class="invalid-feedback"><?php echo $menu_name_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Bike website url</label>
                <input type="text" name="url" class="form-control <?php echo (!empty($url_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $url; ?>">
                <span class="invalid-feedback"><?php echo $url_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Teaser</label>
                <input type="text" name="teaser" class="form-control <?php echo (!empty($teaser_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $teaser; ?>">
                <span class="invalid-feedback"><?php echo $teaser_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Meta title</label>
                <input type="text" name="meta_title" class="form-control <?php echo (!empty($meta_title_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $meta_title; ?>">
                <span class="invalid-feedback"><?php echo $meta_title_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Meta keywords</label>
                <input type="text" name="meta_keywords" class="form-control <?php echo (!empty($meta_keywords_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $meta_keywords; ?>">
                <span class="invalid-feedback"><?php echo $meta_keywords_err; ?></span>
            </div>
            <div class="form-group">
                <label>Meta description</label>
                <input type="text" name="meta_description" class="form-control <?php echo (!empty($meta_description_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $meta_description; ?>">
                <span class="invalid-feedback"><?php echo $meta_description_err; ?></span>
            </div>
            <div class="form-group">
            <input type="file" name="file" value=""/>
          </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-secondary ml-2" value="Reset">
            </div>
        </form>
    </div>    
</body>
</html>