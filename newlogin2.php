<?php
// Iesākt sesiju
session_start();
 
// Pārbauda vai lietotājs jau ir pieslēdzies, ja ir tad pārvirza uz sākuma lapu
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}
 
require_once "config.php";
 
// Definēt vērtības un inicializēt kā tukšas
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = $login_err = "";
 


// Pāstrādā datus kad foma ir apstiprināta
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Pārbauda vai lietotājvārds ir tukšs lauks
    if(empty(trim($_POST["username"]))){
        $username_err = "Lūdzu ievadiet lietotājvārdu.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Pārbauda vai parole ir tukšs lauks
    if(empty(trim($_POST["password"]))){
        $password_err = "Lūdzu ievadiet paroli.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validē ievadītos datus
    if(empty($username_err) && empty($password_err)){

        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("s", $param_username);
            
            $param_username = $username;
            
            if($stmt->execute()){
                $stmt->store_result();
                
                // Pārbauda vai parole eksistē, ja eksistē verificē to
                if($stmt->num_rows == 1){                    
                    $stmt->bind_result($id, $username, $hashed_password);
                    if($stmt->fetch()){
                        if(password_verify($password, $hashed_password)){
                            // Parole ir pareiza, sāk jaunu sesiju
                            session_start();
                            
                            // Saglabā datus sesijas mainīgajos
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            // Pārvirza lietotāju uz sākuma lapu
                            header("location: index.php");
                        } else{
                            // Parole nav pareiza izvada kļūdas ziņojumu
                            $login_err = "Nepareiza parole vai lietotājvārds.";
                        }
                    }
                } else{
                    // Lietotājvārds neeksistē, izvada kļūdas ziņojumu
                    $login_err = "Lietotājvārds neeksistē.";
                }
            } else{
                echo "OOPS! Kaut kas nogāja greizi, lūdzu mēginiet vēlreiz vēlāk.";
            }

            $stmt->close();
        }
    }
    
    $conn->close();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800&display=swap" rel="stylesheet">
    <style>
        body{ 
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            background: -webkit-linear-gradient(left, #7579ff, #b224ef);
            font-family: 'Nunito', sans-serif;
         }
    </style>
</head>

<body>
    <div class="cont">
        <?php 
    if(!empty($login_err)){
        echo '<div class="alert alert-danger">' . $login_err . '</div>';
    }        
    ?>
            <div class="form sign-in">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                <h2>Sign In</h2>
                <label>
        <span>Email Address</span>
        <div class="form-group">
          <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
          <span class="invalid-feedback"><?php echo $username_err; ?></span>
      </div>   
      </label>
                <label>
        <span>Password</span>
        <div class="form-group">
          <input type="password"  name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
          <span class="invalid-feedback"><?php echo $password_err; ?></span>
          </div>
          <input type='hidden' name='action' value='login' />
      </label>
      <div class="form-group">
                <button type="submit" class="submit" value="Login">Login</button>
            </div>
                <p class="forgot-pass">Forgot Password ?</p>

            </form>
            </div>

            <div class="sub-cont">
                <div class="img">
                    <div class="img-text m-up">
                        <h2>New here?</h2>
                        <p>Sign up and discover great amount of new opportunities!</p>
                    </div>
                    <div class="img-text m-in">
                        <h2>One of us?</h2>
                        <p>If you already has an account, just sign in. We've missed you!</p>
                    </div>
                    <div class="img-btn">
                        <span class="m-up">Sign Up</span>
                        <span class="m-in">Sign In</span>
                    </div>
                </div>
                <?php 
    if(!empty($login_err)){
        echo '<div class="alert alert-danger">' . $login_err . '</div>';
    }        
    ?>
                <div class="form sign-up">
                <form action="register.php" method="post" id="form2">

                    <h2>Sign Up</h2>
                    <label>
          <span>Username</span>
          <div class="form-group">
            <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
            <span class="invalid-feedback"><?php echo $username_err; ?></span>
        </div>    
        </label>
                    <label>
          <span>Password</span>
          <div class="form-group">
            <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
            <span class="invalid-feedback"><?php echo $password_err; ?></span>
        </div>
        </label>
                    <label>
          <span>Confirm password</span>
          <div class="form-group">
                <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>     
            <input type='hidden' name='action' value='register' />

       </label>
       <div class="form-group">
                <button type="submit" class="btn btn-primary submit" value="Submit" form="form2">Sign Up</button>
                <button type="reset" class="btn btn-secondary submit" value="Reset">Reset</button>
            </div>
                    
                </div>
            </div>
    </div>
    <script type="text/javascript" src="assets/js/script.js"></script>
<?php include 'footer.php'?>