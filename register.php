<?php
//Pievienojam config failu

require_once "config.php";

$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";



    if(empty(trim($_POST["username"]))){
        $username_err = "Lūdzu ievadiet lietotājvārdu.";
    } elseif(!preg_match('/^[a-zA-Z0-9]+$/', trim ($_POST["username"]))){
        $username_err = "Lietotājvārds var tikai iekļaut burtus un ciparus.";
    } else {
        $sql = "SELECT id FROM users WHERE username = ?";

        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("s", $param_username);

            $param_username = trim($_POST["username"]);

            if($stmt->execute()){
                $stmt->store_result();

                if($stmt->num_rows == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                    echo "OOPS! Kaut kas nogāja gerizi. Lūdzu mēģiniet atkal vēlāk.";
                }

            $stmt->close();
            
        }
    }

    if(empty(trim($_POST["password"]))){
        $password_err = "Lūdzu ievadiet paroli.";
    } elseif(strlen(trim($_POST["password"])) <8){
        $password_err = "Parolei jābūt vismaz ar 8 simboliem.";
    } else {
        $password = trim($_POST["password"]);
    }

    //Validēt apstiprināt paroli
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Lūdzu apstipriniet paroli.";
    }else {
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }

    //Ievades kļūdu pārbaude pirms ievietošanas datubāzē
    if(empty($username_err) && empty($password_er) && empty($confirm_password_err)){

        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";

        if($stmt = $conn->prepare($sql)){

            $stmt->bind_param("ss", $param_username, $param_password);


            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);

            if($stmt->execute()){
                header("location: login.php");
            } else {
                echo "OOPS! Kaut kas nogāja greizi. Lūdzu mēģiniet vēlreiz vēlāk."; 
            }

            $stmt->close();
        }
    }
    $conn->close();

?>
