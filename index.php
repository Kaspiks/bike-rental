<?php
include 'config.php';
$conn = Connect();

session_start();
?>

<?php include 'header.php';?>
        <!-- Banner -->
        <section id="banner">
            <div class="inner">
                <h2>Riteņu noma</h2>
                <p>Vēlviena riteņu nomas<br /> mājaslapa.</p>
                <ul class="actions special">
                    <li><a href="#" class="button primary">Par mums</a></li>
                </ul>
            </div>
            <a href="#one" class="more scrolly">Learn More</a>
        </section>

        <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
    <div class="panel">

        <div class="pricing-plan">
            <img src="icons/cruiser.png" alt="" class="pricing-img">
            <h2 class="pricing-header">Comfort / Cruiser</h2>
            <ul class="pricing-features">
                <li class="pricing-features-item">Paredzēta ikdienas atpūtas braucējam</li>
                <li class="pricing-features-item">Komforts un kontrole vienuviet</li>
            </ul>
            <span class="pricing-price">Piesakies</span>
            <a href="#/" class="pricing-button">Sign up</a>
        </div>

        <div class="pricing-plan">
            <img src="icons/bike.png" alt="" class="pricing-img">
            <h2 class="pricing-header">Hybrid / Commuter</h2>
            <ul class="pricing-features">
                <li class="pricing-features-item">Designed to go far and get there fast.</li>
                <li class="pricing-features-item">Perfects lai dotos apkārt pilsētai, tālākos braucienos</li>
            </ul>
            <span class="pricing-price">Piesakies</span>
            <a href="#/" class="pricing-button is-featured">Sign up</a>
        </div>

        <div class="pricing-plan">
            <img src="icons/smartmotion-xcity.png" alt="" class="pricing-img">
            <h2 class="pricing-header">Mountain / Off-road</h2>
            <ul class="pricing-features">
                <li class="pricing-features-item">Faster than traditional bike, with less effort</li>
                <li class="pricing-features-item">Perfect for mountains and off-road</li>
            </ul>
            <span class="pricing-price">Piesakies</span>
            <a href="#/" class="pricing-button">Sign up</a>
        </div>

    </div>
            </div>
        </section>

        <!-- Two -->
        <section id="two" class="wrapper alt style2">
            <section class="spotlight">
                <div class="image"><img src="images/pic01.jpg" alt="" /></div>
                <div class="content">
                    <h2>Electronic<br /> Comfort / Cruiser </h2>
                    <p>Aliquam ut ex ut augue consectetur interdum. Donec hendrerit imperdiet. Mauris eleifend fringilla nullam aenean mi ligula.</p>
                </div>
            </section>
            <section class="spotlight">
                <div class="image"><img src="images/pic02.jpg" alt="" /></div>
                <div class="content">
                    <h2>Electronic<br /> Hybrid / Commuter </h2>
                    <p>Aliquam ut ex ut augue consectetur interdum. Donec hendrerit imperdiet. Mauris eleifend fringilla nullam aenean mi ligula.</p>
                </div>
            </section>
            <section class="spotlight">
                <div class="image"><img src="images/pic03.jpg" alt="" /></div>
                <div class="content">
                    <h2>Augue eleifend aliquet<br /> sed condimentum</h2>
                    <p>Aliquam ut ex ut augue consectetur interdum. Donec hendrerit imperdiet. Mauris eleifend fringilla nullam aenean mi ligula.</p>
                </div>
            </section>
        </section>

        <!-- Three -->
        <section id="three" class="wrapper style3 special">
            <div class="inner">
                <header class="major">
                    <h2>Accumsan mus tortor nunc aliquet</h2>
                    <p>Aliquam ut ex ut augue consectetur interdum. Donec amet imperdiet eleifend<br /> fringilla tincidunt. Nullam dui leo Aenean mi ligula, rhoncus ullamcorper.</p>
                </header>
                <ul class="features">
                    <li class="icon fa-paper-plane">
                        <h3>Arcu accumsan</h3>
                        <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
                    </li>
                    <li class="icon solid fa-laptop">
                        <h3>Ac Augue Eget</h3>
                        <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
                    </li>
                    <li class="icon solid fa-code">
                        <h3>Mus Scelerisque</h3>
                        <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
                    </li>
                    <li class="icon solid fa-headphones-alt">
                        <h3>Mauris Imperdiet</h3>
                        <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
                    </li>
                    <li class="icon fa-heart">
                        <h3>Aenean Primis</h3>
                        <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
                    </li>
                    <li class="icon fa-flag">
                        <h3>Tortor Ut</h3>
                        <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
                    </li>
                </ul>
            </div>
        </section>

        <!-- CTA -->
        <section id="cta" class="wrapper style4">
            <div class="inner">
                <header>
                    <h2>Arcue ut vel commodo</h2>
                    <p>Aliquam ut ex ut augue consectetur interdum endrerit imperdiet amet eleifend fringilla.</p>
                </header>
                <ul class="actions stacked">
                    <li><a href="#" class="button fit primary">Activate</a></li>
                    <li><a href="#" class="button fit">Learn More</a></li>
                </ul>
            </div>
        </section>
        <?php 

      

$per_page = 20;


if(isset($_GET['page'])) {


$page = $_GET['page'];

} else {


$page = "";
}


if($page == "" || $page == 1) {

$page_1 = 0;

} else {

$page_1 = ($page * $per_page) - $per_page;

}



$post_query_count = "SELECT * FROM bikes";


$find_count = mysqli_query($conn,$post_query_count);
$count = mysqli_num_rows($find_count);

if($count < 1) {


echo "<h1 class='text-center'>No posts available</h1>";




} else {


$count  = ceil($count /$per_page);




$query = "SELECT * FROM bikes LIMIT $page_1, $per_page";
$select_all_posts_query = mysqli_query($conn,$query);
?>
<main class="page-content">
<?php
while($row = mysqli_fetch_assoc($select_all_posts_query)) {
$bike_id = $row['bike_id'];
$bike_name = $row["name"];
$bike_type = $row["type"];
$bike_teaser = $row["teaser"];
$bike_img = $row['bike_img'];
$bike_content = substr($row["content"],0,400);

?>



<!-- First Blog Post -->

<div class="card" style="background-image: url('<?php echo $bike_img ?>');">
    <div class="content-card">
        <h1 class="title"><?php echo $bike_name ?></h2>
        <h2 class="smth"><?php echo $bike_type ?></h2>
        <p class="teaser"><?php echo $bike_teaser ?></p><a href="bike?id=<?php echo $bike_id; ?>" class="button fit">View  bike</a>
    </div>
</div>


<?php }  } ?>
</main>

           


        <section id="cta" class="wrapper style4">
            <div class="inner">
                <header>
                    <h2>Arcue ut vel commodo</h2>
                    <p>Aliquam ut ex ut augue consectetur interdum endrerit imperdiet amet eleifend fringilla.</p>
                </header>
                <ul class="actions stacked">
                    <li><a href="#" class="button fit primary">Activate</a></li>
                    <li><a href="#" class="button fit">Learn More</a></li>
                </ul>
            </div>
        </section>

<?php include 'footer.php';?>