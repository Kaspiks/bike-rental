<!DOCTYPE HTML>

<html>

<head>
    <script src="https://cdn.tiny.cloud/1/s807tvmg6k3efm8508qsp64hs4z173va5tywon1ityb681i2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <title>Elektrisko riteņu noma</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="https://fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
</head>

<body class="landing is-preload">

    <!-- Page Wrapper -->
    <div id="page-wrapper">
            <!-- Header -->
<header id="header" class="alt">
            <h1><a href="index.php">Riteņu noma</a></h1>
            <nav id="nav">
                <ul>
                    <li class="special">
                        <a href="#menu" class="menuToggle"><span>Menu</span></a>
                        <div id="menu">
                            <ul>
                                <?php
                                if(isset($_SESSION["loggedin"])){
                                    ?>
                                <li><a href="index">Home</a></li>
                                <li><a href="generic">Generic</a></li>
                                <li><a href="elements">Elements</a></li>
                                    <li class="dropbtn" onclick="myFunction('m1')">Menu</li>
                                    <div class="dropdown-content" id="m1">
                                        <li class="dropbtn" onclick="myFunction('m2')">Riteņi</li>
                                        <div class="dropdown-content" id="m2">
                                            <a href="manage-bikes">Pārskatīt riteņus</a>
                                            <a href="enterbike">Pievienot riteni</a>
                                        </div>
                                        <li class="dropbtn" onclick="myFunction('m3')">Braucēji</li>
                                        <div class="dropdown-content" id="m3">
                                            <a href="manage-employees">Pārskatīt braucējus</a>
                                            <a href="enterdriver">Pievienot braucēju</a>
                                        </div>
                                        <li class="dropbtn" onclick="myFunction('m4')">Iznomātie riteņi</li>
                                        <div class="dropdown-content" id="m4">
                                            <a href="manage-rentals">Pārskatīt nomas līgumus</a>
                                        </div>
                                    </div>
                                <li><a href="logout.php">Log out</a></li>
                                <li><a href="#">Welcome <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></a></li>
                                                                <?php
                                } else{
                                ?>
                                <li><a href="index.php">Home</a></li>
                                    <li><a href="generic.php">Generic</a></li>
                                    <li><a href="elements.php">Elements</a></li>
                                    <li><a href="newlogin2.php">Pieslēgties</a></li>
                                    <?php
                                }
                                ?>       
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </header>