<?php
// Iesākt sesiju
session_start();
 
// Pārbauda vai lietotājs jau ir pieslēdzies, ja ir tad pārvirza uz sākuma lapu
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}
 

require_once "config.php";
 
// Definēt vērtības un inicializēt kā tukšas
$username = $password = "";
$username_err = $password_err = $login_err = "";
 
// Pāstrādā datus kad foma ir apstiprināta
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Pārbauda vai lietotājvārds ir tukšs lauks
    if(empty(trim($_POST["username"]))){
        $username_err = "Lūdzu ievadiet lietotājvārdu.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Pārbauda vai parole ir tukšs lauks
    if(empty(trim($_POST["password"]))){
        $password_err = "Lūdzu ievadiet paroli.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validē ievadītos datus
    if(empty($username_err) && empty($password_err)){

        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = $mysqli->prepare($sql)){
            $stmt->bind_param("s", $param_username);
            
            $param_username = $username;
            
            if($stmt->execute()){
                $stmt->store_result();
                
                // Pārbauda vai parole eksistē, ja eksistē verificē to
                if($stmt->num_rows == 1){                    
                    $stmt->bind_result($id, $username, $hashed_password);
                    if($stmt->fetch()){
                        if(password_verify($password, $hashed_password)){
                            // Parole ir pareiza, sāk jaunu sesiju
                            session_start();
                            
                            // Saglabā datus sesijas mainīgajos
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            // Pārvirza lietotāju uz sākuma lapu
                            header("location: welcome.php");
                        } else{
                            // Parole nav pareiza izvada kļūdas ziņojumu
                            $login_err = "Nepareiza parole vai lietotājvārds.";
                        }
                    }
                } else{
                    // Lietotājvārds neeksistē, izvada kļūdas ziņojumu
                    $login_err = "Lietotājvārds neeksistē.";
                }
            } else{
                echo "OOPS! Kaut kas nogāja greizi, lūdzu mēginiet vēlreiz vēlāk.";
            }

            $stmt->close();
        }
    }
    
    $mysqli->close();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="/assets/js/main.js"></script>

    <style>
        body{ 	background: #f6f5f7;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	font-family: 'Montserrat', sans-serif;
    height: 100vh;
	margin: 0 0 0;}
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>
<body>


<div class="container" id="container">
    <?php 
    if(!empty($login_err)){
        echo '<div class="alert alert-danger">' . $login_err . '</div>';
    }        
    include "register.php"
    ?>
	<div class="form-container sign-up-container">
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
			<h1>Create Account</h1>
			<div class="social-container">
				<a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
				<a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
				<a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
			</div>
			<span>or use your email for registration</span>
            <div class="form-group">
                <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-secondary ml-2" value="Reset">
            </div>
			<button>Sign Up</button>
		</form>
	</div>
	<div class="form-container sign-in-container">
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        
			<h1>Sign in</h1>
			<div class="social-container">
				<a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
				<a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
				<a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
			</div>
			<span>or use your account</span>
            <div class="form-group">
			<input type="text" placeholder="name" name="username" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
            <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>
            <div class="form-group">
            <input type="password" placeholder="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
            <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
			<a href="#">Forgot your password?</a>
			<button>Sign In</button>

		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>Welcome Back!</h1>
				<p>To keep connected with us please login with your personal info</p>
				<button class="ghost" id="signIn">Sign In</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1>Hello, Friend!</h1>
				<p>Enter your personal details and start journey with us</p>
				<button class="ghost" id="signUp">Sign Up</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>
