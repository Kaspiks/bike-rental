<?php
function redirect($location){
    header("Location:" . $location);
    exit;
}

function stmtConnection(){
    global $conn;
    return mysqli_stmt_init($conn);
}



function confirmQuery($result) {
    
    global $conn;

    if(!$result ) {
          
          die("QUERY FAILED ." . mysqli_error($conn));
   
          
      }
    

}


function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    confirmQuery($result);
    return $result;
}

function fetchRecords($result){
    return mysqli_fetch_array($result);
}

function count_records($result){
    return mysqli_num_rows($result);
}


function escape($string) {

    global $conn;
    
    return mysqli_real_escape_string($conn, trim($string));
    
    
    }
    
?>