<?php

require 'config.php';
$conn = Connect();
session_start();



include 'header.php';?>
<script>

var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

tinymce.init({
  selector: 'textarea#full-featured-non-premium',
  plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
  imagetools_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: true,
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  branding: false,
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  file_picker_callback: function (callback, value, meta) {
    /* Provide file and text for the link dialog */
    if (meta.filetype === 'file') {
      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    }

    /* Provide image and alt text for the image dialog */
    if (meta.filetype === 'image') {
      callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
    }

    /* Provide alternative source and posted for the media dialog */
    if (meta.filetype === 'media') {
      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    }
  },
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 300,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  contextmenu: 'link image imagetools table',
  skin: "oxide-dark",
content_css: "dark",
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
 });


    </script>
      <!-- Navigation -->


    <div class="container rental-container" style="margin-top: 65px;" >
    <div class="col-md-7" style="float: none; margin: 0 auto;">
      <div class="form-area">
        <form class="rental-form" role="form" action="enterbikeback.php" enctype="multipart/form-data" method="POST">
        <br style="clear: both">
          <h3 style="margin-bottom: 25px; text-align: center; font-size: 30px;"> Please Provide Your Car Details. </h3>

          <div class="form-group">
          <label for="name">Riteņa nosaukums</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="name " required autofocus="">
          </div>

          <div class="form-group">
         <label for="content">Riteņa apraksts</label>
         <form method="post">
      <textarea id="full-featured-non-premium" name= "content"></textarea>
    </form> 

          <div class="form-group">
          <label for="type">Riteņa tips</label>
            <select name="type" class="form-control" id="type" placeholder="type" required>
              <option value="">Izvēlies riteņa tipu</option>
              <option value="Komforta">Komforta</option>
              <option value="Hibrīds">Hibrīds</option>
              <option value="Kalna">Kalna</option>
            </select>
          </div>

          <div class="form-group">
          <label for="menu_name">Menu nosaukums</label>
            <input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="menu_name" required>
          </div>

          <div class="form-group">
          <label for="url">URL adrese</label>
            <input type="text" class="form-control" id="url" name="url" placeholder="url" required>
          </div>

          <div class="form-group">
          <label for="teaser">Ieskats tekstā</label>
            <input type="text" class="form-control" id="teaser" name="teaser" placeholder="teaser" required>
          </div>
          <div class="form-group">
          <label for="meta_title">Meta virsraksts</label>
            <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="meta_title" required>
          </div>
          <div class="form-group">
          <label for="meta_keywords">Meta atslēgas vārdi</label>
            <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" placeholder="meta_keywords" required>
          </div>
          <div class="form-group">
          <label for="meta_description">Meta apraksts</label>
            <input type="text" class="form-control" id="meta_description" name="meta_description" placeholder="meta_description" required>
          </div>

          <div class="form-group img-input">
            <div class="inputfile-box">
              <input name="uploadedimage" type="file" id="file" class="inputfile" onchange='uploadFile(this)'>
              <label for="file">
                <span id="file-name" class="file-box"></span>
                <span class="file-button">
                  <i class="fa fa-upload" aria-hidden="true"></i>
                  Select File
                </span>
              </label>
            </div>
          </div>
           <button type="submit" id="submit" name="submit" class="button primary rent"> Submit for Rental</button>    
        </form>
      </div>
    </div>
    </div>
</div>
<?php include 'footer.php';