<?php

//Incializē sesiju
session_start();

$_SESSION = array();

session_destroy();

//Pārvirzīt uz login lapu
header("location: login.php");
exit;
?>