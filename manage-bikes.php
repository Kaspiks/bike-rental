
<?php
require 'config.php';
$conn = Connect();
session_start();

if(isset($_POST['checkBoxArray'])) {
    foreach($_POST['checkBoxArray'] as $bikeValueId ){
        $bulk_options = $_POST['bulk_options'];
        switch($bulk_options) {
            case 'delete':
                $query = "DELETE FROM bikes WHERE bike_id = {$bikeValueId}  ";
                $update_to_delete_status = mysqli_query($conn,$query);
                break;
            case 'clone':
                $query = "SELECT * FROM bikes WHERE bike_id = '{$bikeValueId}' ";
                $select_post_query = mysqli_query($conn, $query);
                while ($row = mysqli_fetch_array($select_post_query)) {
                    $name = $row["name"];
                    $content = $row["content"];
                    $type = $row["type"];
                    $image = $row["bike_img"];
                    $menu_name = $row["menu_name"];
                    $url = $row["url"];
                    $teaser = $row["teaser"];
                    $meta_title = $row["meta_title"];
                    $meta_keywords = $row["meta_keywords"];
                    $meta_description = $row["meta_description"];

                }
                $query = "INSERT into bikes(name,content,type,bike_img,menu_name,url,teaser,meta_title,meta_keywords,meta_description) VALUES('" . $name . "','" . $content . "','".$type."','" . $image . "','" . $menu_name . "','" . $url . "','" . $teaser . "','" . $meta_title ."','" . $meta_keywords ."','" . $meta_description ."')";
                $copy_query = mysqli_query($conn, $query);

                if(!$copy_query ) {

                    die("QUERY FAILED" . mysqli_error($conn) . $query);
                }
                break;
        }
    }
}
include 'header.php'
?>

    <form class="manage-b" action="" method='post'>
        <table class="table table-bordered table-hover">
            <div id="bulkOptionContainer" >
                <select class="form-control" name="bulk_options" id="">
                <option value="">Izvēlieties opciju</option>
                <option value="delete">Dzēst</option>
                <option value="clone">Klonēt</option>
            </select>
            </div>
            <div class="col-xs-4">
                <input type="submit" name="submit" class="btn btn-success btn-ap bg" value="Apstiprināt">
                <a class="btn-add" href="enterbike.php">Pievienot</a>
            </div>
            <thead>
                <tr>
                    <th><input id="selectAllBoxes" type="checkbox"></th>
                    <th>Id</th>
                    <th> Nosaukums</th>
                    <th> Tips </th>
                    <th> Bilde </th>
                    <th> Menu nosakums</th>
                    <th> URL adrese </th>
                    <th> Teaseris </th>
                    <th> Meta nosaukums </th>
                    <th> Meta atslēgas vārdi </th>
                    <th> Meta apraksts </th>
                    <th> Apskatit</th>
                    <th> Rediģēt</th>
                </tr>
            </thead>

            <tbody>
                <?php
        $query = "SELECT * FROM bikes ORDER BY bike_id DESC ";
        $select_bikes = mysqli_query($conn,$query);
        while($row = mysqli_fetch_assoc($select_bikes )) {
            $bike_id = $row['bike_id'];
            $bike_name = $row["name"];
            $bike_type = $row["type"];
            $bike_img = $row["bike_img"];
            $bike_menu_name = $row["menu_name"];
            $bike_url = $row["url"];
            $bike_teaser = $row["teaser"];
            $bike_meta_title = $row["meta_title"];
            $bike_meta_keywords = $row["meta_keywords"];
            $bike_meta_description = $row["meta_description"];
            echo "<tr>";
            ?>    

   <td class="cbx"><input id="cbx" type='checkbox' name='checkBoxArray[]' value='<?php echo $bike_id; ?>'  /><label for="cbx"></label><svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                <path d="M2 8.36364L6.23077 12L13 2"></path>
            </svg></td>
            <defs>
                <filter id="goo">
                    <fegaussianblur in="SourceGraphic" stddeviation="4" result="blur"></fegaussianblur>
                    <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 22 -7" result="goo"></fecolormatrix>
                    <feblend in="SourceGraphic" in2="goo"></feblend>
                </filter>
            </defs>
        </svg>
                   

           <td><?php echo $bike_id; ?></td>
           <td><?php echo $bike_name; ?></td>
           <td><?php echo $bike_type; ?></td>
           <td><img width='100' src='<?php echo $bike_img?>' alt='image'></td>
           <td><?php echo $bike_menu_name; ?></td>
           <td><?php echo $bike_url; ?></td>
           <td><?php echo $bike_teaser; ?></td>
           <td><?php echo $bike_meta_title; ?></td>
           <td><?php echo $bike_meta_keywords; ?></td>
           <td><?php echo $bike_meta_description; ?></td>



            <td>
                <a class='btn-add sm' href='bike?id=<?php echo $bike_id;?>'>Apskatīt</a>
            </td>
            <td>
                <a class='btn-add sm' href='edit-bike?id=<?php echo $bike_id;?>'>Rediģēt</a>
            </td>

            </tr>
            <?php

        }

        ?>
            </tbody>
        </table>
    </form>
    </div>

    <?php
include 'footer.php';?>