<!DOCTYPE html>
<html>

<?php 
session_start();

require 'config.php';
$conn = Connect();

include 'header.php';

$rental_id = $_GET["id"];

$sql1 = 
"SELECT * FROM rentedbikes rb 
LEFT JOIN bikes b ON `rb`.`bike_id` = `b`.`bike_id`
LEFT JOIN employees e ON `rb`.`employee_id` = `e`.`employee_id`
WHERE `rb`.`id` = $rental_id";
$result1 = $conn->query($sql1);

if (mysqli_num_rows($result1) > 0) {
    while($row = mysqli_fetch_assoc($result1)) {
        $id = $row["id"];
        $bike_name = $row["name"];
        $employee_name = $row["employee_name"];
        $employee_surname = $row["employee_surname"];
        $employee_email = $row["email"];
        $employee_phone = $row["phone"];
        $booking_date = $row["booking_date"];
        $rent_start_date = $row["rent_start_date"];
        $rent_end_date = $row["rent_end_date"];
        $return_status = $row["return_status"];


if (!$result1){
    die("Couldnt enter data: ".$conn->error);
}


?>
    <div class="container">
        <div class="jumbotron">
            <h1 class="text-center" style="color: green;"><span class="glyphicon glyphicon-ok-circle"></span> Booking Confirmed.</h1>
        </div>
    </div>
    <br>
 

    <h3 class="text-center"> <strong>Your Order Number:</strong> <span style="color: blue;"><?php echo "$id"; ?></span> </h3>


    <div class="container">
        <div class="box">
            <div class="col-md-10" style="float: none; margin: 0 auto; text-align: center;">
                <br>
                <h3 style="color: orange;">Invoice</h3>
                <br>
            </div>
            <div class="col-md-10" style="float: none; margin: 0 auto; ">
                <h4> <strong>Riteņa Nosaukums: </strong> <?php echo $bike_name; ?></h4>
                <br>
                <h4> <strong>Īres Datums: </strong> <?php echo date("Y-m-d"); ?> </h4>
                <br>
                <h4> <strong>Nomas Sākuma Datums: </strong> <?php echo $rent_start_date; ?></h4>
                <br>
                <h4> <strong>Atgriešanas Datums: </strong> <?php echo $rent_end_date; ?></h4>
                <br>
                <h4> <strong>Darbinieka Vārds: </strong> <?php echo $employee_name; ?> </h4>
                <br>
                <h4> <strong>Darbinieka Uzvārds: </strong> <?php echo $employee_surname; ?> </h4>
                <br>
                <h4> <strong>Darbinieka Saziņas E-pasts:</strong>  <?php echo $employee_email; ?></h4>
                <br>
                <h4> <strong>Darbinieka Saziņas Tālrunis:</strong> +371 <?php echo  $employee_phone; ?></h4>
                <br>
            </div>
        </div>
    </div>
    <?php
}
}
    
include 'footer.php';